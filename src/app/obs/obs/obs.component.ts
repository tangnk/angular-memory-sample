import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/internal/Observable';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { SearchItem } from 'src/app/models/search-item';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-obs',
  templateUrl: './obs.component.html',
  styleUrls: ['./obs.component.css']
})
export class ObsComponent implements OnInit {

  public loading: boolean = false;
  public results: Observable<SearchItem[]>;
  public searchField: FormControl;

  constructor(private itunes: SearchService) { }

  ngOnInit(): void {
    this.searchField = new FormControl();
    this.results = this.searchField.valueChanges.pipe(
      debounceTime(400),
      distinctUntilChanged(),
      tap(_ => (this.loading = true)),
      switchMap(term => this.itunes.search(term)),
      tap(_ => (this.loading = false))
    );
  }

  doSearch(term: string): void {
    this.itunes.search(term);
  }
}

