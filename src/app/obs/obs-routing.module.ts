import { ObsComponent } from './obs/obs.component';
import { Routes } from '@angular/router';
export const obsRoutes: Routes = [
    { path: '', component: ObsComponent }
];
