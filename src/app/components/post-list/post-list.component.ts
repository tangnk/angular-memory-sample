import { ResponseListMessage } from './../../models/response-list-message';
import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { PostService } from 'src/app/services/post.service';
import { Observable } from 'rxjs';
import { PostExtendService } from 'src/app/services/post-extend.service';
import { shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostListComponent implements OnInit, OnDestroy {
  post$: Observable<ResponseListMessage<any[]>>;
  req: any;
  data: any;
  page: number;

  constructor(private postService: PostService, private postExtendService: PostExtendService, private cdr: ChangeDetectorRef) {
    this.page = 1;
    this.req = {
      'Method': {
        'Method': 'POST'
      },
      'Url': '/api/processing-queue/search',
      'Module': 'HC',
      'ContentType': 'application/json',
      'AcceptType': 'application/json',
    };
  }

  ngOnInit(): void {
    this.search(null);
  }

  search(previous: boolean = false, next: boolean = false) {
    if (previous) {
      this.page = this.page > 1 ? this.page - 1 : this.page;
    }else if (next) {
      this.page += 1;
    }

    this.data = `{\"person_code\":\"\",\"full_name\":\"\",\"gender\":\"\",\"date_of_birth\":\"\",\"area_id\":\"00000000-0000-4000-0000-000000000000\",\"work_queue_type_rcd\":\"CLI\",\"fr_created_date_time\":\"2020-09-08T00:00:00\",\"to_created_date_time\":\"2020-09-09T23:59:59\",\"search_query\":\"\",\"active_flag\":[],\"arr_active_flag\":null,\"include_hour\":true,\"insurance_flag\":\"\",\"page\":${this.page},\"pageSize\":5}`;
    this.req.Data = this.data;
    //
    this.post$ = this.postService.getAll('https://his.lapolo.com:6001/api/adapter/execute', this.req);


    //
    // this.postExtendService.getAll('https://his.lapolo.com:6001/api/adapter/execute', this.req, this.page);
    // this.post$ = this.postExtendService.post$;
  }

  gotoPage() {
    this.page = this.page;
    this.search(null);
  }

  previous() {
    this.page -= 1;
    if (this.page === 0) {
      this.page = 1;
    }
    this.search(null);
  }

  next() {
    this.page += 1;
    this.search(null);
  }

  // ngOnDestroy() {
  //   if (this.destroy$) {
  //     this.destroy$.next();
  //     this.destroy$.complete();
  //     console.log('PostListComponent destroy');
  //   }
  // }

  ngOnDestroy() {
    console.log('PostListComponent destroy');
  }
}
