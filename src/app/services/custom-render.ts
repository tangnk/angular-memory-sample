export function runOnNextRender(cb: () => void, delayTime: number = 0) {
    setTimeout(cb, delayTime);
}
