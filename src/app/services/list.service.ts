import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

export class Item {
    email: string;
}

@Injectable({
    providedIn: 'root'
})
export class ListService {
    contentType = 'application/json';
    authorization = 'bearer 00000000-0000-0000-0000-000000000001_c1a1df2a82204d059ddc50cfc6720140';
    ver = '01.09.20.003';
    req: any;
    data: any;

    constructor(private readonly http: HttpClient) {
        this.req = {
            'Method': {
                'Method': 'POST'
            },
            'Url': '/api/processing-queue/search',
            'Module': 'HC',
            'ContentType': 'application/json',
            'AcceptType': 'application/json',
        };
    }

    getList(page: number = 1) {
        return this.http.get('https://reqres.in/api/users?page=' + page).pipe(map((response: any) => response.data.map(i => {
            const newItem = new Item();
            newItem.email = i.email;
            return newItem;
        })));
    }

    getList2(page: number = 1) {
        let hd: HttpHeaders = new HttpHeaders({
            'Content-Type': this.contentType,
            'Authorization': this.authorization,
            'ver': this.ver
        });
        this.data = `{\"person_code\":\"\",\"full_name\":\"\",\"gender\":\"\",\"date_of_birth\":\"\",\"area_id\":\"00000000-0000-4000-0000-000000000000\",\"work_queue_type_rcd\":\"CLI\",\"fr_created_date_time\":\"2020-09-08T00:00:00\",\"to_created_date_time\":\"2020-09-09T23:59:59\",\"search_query\":\"\",\"active_flag\":[],\"arr_active_flag\":null,\"include_hour\":true,\"insurance_flag\":\"\",\"page\":${page},\"pageSize\":5}`;
        this.req.Data = this.data;
        return this.http.post('https://his.lapolo.com:6001/api/adapter/execute', this.req, { headers: hd })
            .pipe(map((res: any) => res.Data));
        // return this.baseHttp.post('https://his.lapolo.com:6001/api/adapter/execute', this.req).pipe(map((res: any) => res.Data));
    }

    // handleError(error) {
    //     let errorMessage = '';
    //     if (error.error instanceof ErrorEvent) {
    //         // Get client-side error
    //         errorMessage = error.error.message;
    //     } else {
    //         // Get server-side error
    //         errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    //     }
    //     window.alert(errorMessage);
    //     return throwError(errorMessage);
    // }
}
