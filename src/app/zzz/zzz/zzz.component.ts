import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-zzz',
  templateUrl: './zzz.component.html',
  styleUrls: ['./zzz.component.css']
})
export class ZzzComponent implements OnInit, OnDestroy {

  constructor() { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    console.log('zzz destroy');
  }

}
