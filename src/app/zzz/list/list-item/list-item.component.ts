import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnInit, ChangeDetectionStrategy, Input, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListItemComponent implements OnInit, OnDestroy {

  @Input() item: any;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    console.log('zzz list item destroy');
  }

}
