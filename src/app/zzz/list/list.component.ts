import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs';
import { ListService } from 'src/app/services/list.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListComponent implements OnInit, OnDestroy {

  items$: Observable<any>;
  data: any[];
  currentPage = 1;

  constructor(private readonly listService: ListService, private readonly cdr: ChangeDetectorRef) {
  }

  ngOnInit(): void {
    this.items$ = this.listService.getList2(this.currentPage);
  }

  onNext(): void {
    this.currentPage += 1;
    this.items$ = this.listService.getList2(this.currentPage);
  }

  onPrev(): void {
    this.currentPage -= 1;
    this.items$ = this.listService.getList2(this.currentPage);
  }

  onGo(): void {
    this.items$ = this.listService.getList2(this.currentPage);
  }

  ngOnDestroy(): void {
    console.log('zzz list destroy');
  }

}
