import { FormsModule } from '@angular/forms';
import { ZzzComponent } from './zzz/zzz.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { ListItemComponent } from './list/list-item/list-item.component';

const zzzRoutes: Routes = [
  { path: '', component: ZzzComponent }
];

@NgModule({
  declarations: [ZzzComponent, ListComponent, ListItemComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(zzzRoutes)
  ]
})
export class ZzzModule { }
